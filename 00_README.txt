### Stage-0 ###
### raw calls based on wgs.sv.bu.tgz ###
echo lftp
ftp://ADSP:434xxC5H4tAJ6DtEjS5K@faraday.pcbi.upenn.edu/analysis_group/PilotFamily_VCFs/structural_variant
-e \"get -c wgs.sv.bu.tgz\" | qsub

### Stage-1 ###
### fix missing some samples in Delly call files ###
#expectd 186, actual 180
#cat ../../../ADSP_WGSfam_pilot.srr.txt | grep WashU | cut -f13 >WashU.pilot.srr
#ls washu/* | grep -o "SRR[0-9]*" >WashU.delly.srr
#grep -v WashU.pilot.srr -f WashU.delly.srr >WashU.miss.srr
#grep -f WashU.miss.srr ../../../ADSP_WGSfam_pilot.srr.txt
#532	A-UPN-UP000152	A-UPN-UP000152-BL-UPN-1425	UP0004F	A-UPN-UP000159
#A-UPN-UP000160	1	0	92	1	Caucasian	WashU
#SRR1047713
#536	A-UPN-UP000175	A-UPN-UP000175-BL-UPN-1320	UP0005F	A-UPN-UP000171
#A-UPN-UP000170	0	1	78	1	Caucasian	WashU
#SRR1104792
#544	A-UPN-UP000261	A-UPN-UP000261-BL-UPN-12047	UP0007F	A-UPN-UP000264
#A-UPN-UP000263	0	2	77	1	Caucasian	WashU
#SRR1104774
#545	A-UPN-UP000265	A-UPN-UP000265-BL-UPN-24181	UP0007F	A-UPN-UP000264
#A-UPN-UP000263	0	2	80	1	Caucasian	WashU
#SRR1104759
#547	A-UPN-UP000267	A-UPN-UP000267-BL-UPN-68108	UP0007F	A-UPN-UP000268
#A-UPN-UP000259	0	2	67	1	Caucasian	WashU
#SRR1104762
#548	A-UPN-UP000276	A-UPN-UP000276-BL-UPN-37556	UP0008F	A-UPN-UP000274
#A-UPN-UP000275	1	2	69	1	Caucasian	WashU
#SRR1057420
##let's just fill them in with empty file for now
grep "#" WashU/adsp.sv.wgs.all.SRR990604.delly.2014_1030.del.vcf >adsp.sv.wgs.all.SRR1047713.delly.2014_1030.del.vcf

### Have to fix errors in ADSP_WGSfam.txt ###
#keep only the first 585 lines which are sequenced
#correct 13 obvious label errors such as
#A-CUHS-CU000445-BL-COL-32024BL2-BL-COL-32024BL2 ->
#A-CUHS-CU000445-BL-COL-32024BL2
#save it as ADSP_WGSfam_pilot.txt
#load it in R
#pilot=read.table("ADSP_WGSfam_pilot.txt", header=T, sep="\t")
#adsp2srr=read.table("ADSP2SRR.txt")
#pilot$SRR=adsp2srr$V2[match(pilot$SampleID, adsp2srr$V1)]
#pilot[which(is.na(pilot$SRR)),] #show problematic lines, should be none
#write.table(pilot,file="ADSP_WGSfam_pilot.srr.txt",sep="\t",quote=F)

### clean up sample center mis-classification ###
#which three is mixed into WashU calls?
#change to directory
cd ~/adsp_rawcalls/Breakdancer
#Find Sample ID of Center in ADSP_WGSfam_pilot.txt
cat ~/adsp_dscore/ADSP_WGSfam_pilot.txt | grep Broad | cut -f1 >Broad.si
#Find SRR of Broad.si, this should be 3 instead of 9
grep -f Broad.si ~/adsp_rawcalls/ADSP2SRR.txt | cut -f2 >Broad.srr 
#Find SRR of all 229 callset from Broad (instead of 232 as expected by
WGSfam_pilot.txt)
ls Broad/*.bdmax| grep -o  "SRR[0-9]*" >Broad.srr.bdmax
#Find SRR in callset files for Broad but not in WGSfam_pilot.txt
grep -v -f Broad.srr.bdmax Broad.srr >Broad.srr.extra
#Find those missed’s SampleID, seems to be the duplicates causing problem
grep -f Broad.srr.extra ~/adsp_rawcalls/ADSP2SRR.txt
#A-CUHS-CU001167-BL-COL-32167BL1     SRR1024251
#A-CUHS-CU001163-BL-COL-32131BL1     SRR987678
#A-CUHS-CU001166-BL-COL-32166BL1     SRR988139
#A-CUHS-CU001163-BL-COL-32131BL1w     SRR1158261
#A-CUHS-CU001166-BL-COL-32166BL1w     SRR1158848
#A-CUHS-CU001167-BL-COL-32167BL1w     SRR1158851
#A-CUHS-CU001167-BL-COL-32167BL1r     SRR1200982
#A-CUHS-CU001163-BL-COL-32131BL1r     SRR1200985
#A-CUHS-CU001166-BL-COL-32166BL1r     SRR1200989
#Fix missing, move file from WashU to Broad and Baylor accordingly
mv WashU/*SRR1200982* WashU/*SRR1200985* WashU/*SRR1200989* Broad
mv WashU/*SRR1024251* WashU/*SRR987678* WashU/*SRR988139* Baylor

### merge lumpy calls into one bedpe file
for srr in `cat ../ADSP_WGSfam_pilot.srr.txt | grep WashU | cut -f13`; do cat WashU/*$srr*.bedpe >WashU/$srr.lumpy.merged; done;
for srr in `cat ../ADSP_WGSfam_pilot.srr.txt | grep Baylor | cut -f13`; do cat Baylor/*$srr*.bedpe >Baylor/$srr.lumpy.merged; done;
for srr in `cat ../ADSP_WGSfam_pilot.srr.txt | grep Broad | cut -f13`; do cat Broad/*$srr*.bedpe >Broad/$srr.lumpy.merged; done;

#Check the Breakdancer numbers
ls Broad/*.bdmax | wc -l #232
ls Baylor/*.bdmax | wc -l #166
ls WashU/*.bdmax | wc -l #186
#Check the CNVnator numbers
ls Broad/*.calls | wc -l 
ls Baylor/*.calls | wc -l 
ls WashU/*.calls | wc -l 
#Check the Delly numbers
ls Broad/*.vcf | wc -l 
ls Baylor/*.vcf | wc -l 
ls WashU/*.vcf | wc -l
#check the Lumpy numbers
ls Broad/*.merged | wc -l
ls Baylor/*.merged | wc -l
ls WashU/*.merged | wc -l

### Stage-2 ###
#convert all SV caller output into bed file
echo "for file in `ls */*.calls`; do multi2bed.PY -f CN $file; done;" | bsub -J m2b_CN 
echo "for f in `ls /home/xiac/adsp_rawcalls/Delly/*/*.vcf`; do vcf2bed.sh $f; gzip ${f%.vcf}.bed; done;" | bsub -J MergeDL $f; #mergeDelly.sh
echo "for file in `ls */*.merged`; do multi2bed.PY -f LP $file; done;" | bsub -J m2b_LP 

### Stage-3 ###
#convert all SV caller output into RData file
#applicable scripts
#-rw-r--r-- 1 xiac 1.5K Nov  7 18:18 convertBreakdancer.R
#-rw-r--r-- 1 xiac 1.2K Nov  9 20:07 convertCNVnator.R
#-rw-r--r-- 1 xiac 1.7K Nov  9 18:45 convertDelly.R
#-rw-r--r-- 1 xiac 1.2K Nov  7 18:16 convertHaplotypeCaller.R
#-rw-r--r-- 1 xiac 1.5K Nov  7 16:43 convertLumpy.R
#-rw-r--r-- 1 xiac  890 Nov  7 16:39 convertPindel.R
#-rwxr-xr-x 1 xiac 1.2K Nov  8 17:54 convertSWAN.R*
bsub -J cHC "R --no-save <convertHaplotypeCaller.R >convertHaplotypeCaller.log 2>&1"
bsub -J cDL "R --no-save <convertDelly.R >convertDelly.log 2>&1"
convertCaller.sh

### Stage-4 ###
#add d-score to all raw calls
#applicable scripts
#-rw-rw-r-- 1 xiac xiac 953 Nov 11 16:15 adspDscore.Breakdancer.sh
#-rw-rw-r-- 1 xiac xiac 937 Nov  9 19:34 adspDscore.CNVnator.sh
#-rw-rw-r-- 1 xiac xiac 946 Nov 11 16:17 adspDscore.Delly.sh
#-rw-r--r-- 1 xiac xiac 944 Nov  9 19:39 adspDscore.HaplotypeCaller.sh
#-rw-rw-r-- 1 xiac xiac 933 Nov  9 19:40 adspDscore.Lumpy.sh
#-rw-r--r-- 1 xiac xiac 935 Nov  9 19:41 adspDscore.Pindel.sh
#-rw-r--r-- 1 xiac xiac 945 Nov 11 16:18 adspDscore.SWAN.sh
#adsplib.r
#adspSharingAll.r

### Stage-5 ###
#compute SensMat
ADSP_Sensitivity.R
