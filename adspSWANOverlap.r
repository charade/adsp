#!/usr/bin/env Rscript
library(plyr)
library("BSgenome.Hsapiens.UCSC.hg19")
genome = BSgenome.Hsapiens.UCSC.hg19
load("SWAN.allcenters.filteredcalls.overlap.RData")
nprint=length(HitList) #nprint=7
EventVCF=rbind.fill(lapply(seq(nprint),decomposeEvents,HitList,"HitsAll"))
EventVCFFile="ADgenes.SWAN.allcenters.HitsAll.vcf"
EventBEDFile="ADgenes.SWAN.allcenters.HitsAll.bed"
write.table(EventVCF,file=EventVCFFile,quote=FALSE,row.names=FALSE,sep="\t")
write.table(EventVCF[,c(1,2,6)],file=EventBEDFile,quote=FALSE,row.names=FALSE,sep="\t")
save.image("SWAN.allcenters.filteredcalls.overlap.RData")
quit()

setwd("/Users/charlie/Dropbox/ADSP")
par(mar=c(2, 2, 2, 2)) #bottom, left, top, right

Len=End-Start
ismember<-function(v,s){
    mm=match(v,s)
    !is.na(mm)
}

findBad<-function(){
    bad=sibshare==0 & sibcount>=5
    bad=bad | (sibcount>3 & sibcount<6 & L< -5)
    bad=bad | (sibcount>=6 & L< 0)
    bad=bad | (Type=="INS" & L<5 & popFreq>0.05)
    bad=bad | (Type=="INS" & L<0 & popFreq<0.05)

    ndisc=table(Sample[Method=="disc"])
    badsamples=names(ndisc)[which(ndisc>median(ndisc)+6*1.4826*mad(ndisc))]
    bad=bad | (ismember(Sample,badsamples) & Method=="disc" & L<10)

    nldx=table(Sample[Method=="ldx"])
    badsamples=names(nldx)[which(nldx>median(nldx)+6*1.4826*mad(nldx))]
    bad=bad | (ismember(Sample,badsamples) & Method=="ldx" & L<10)

    nbigd=table(Sample[Method=="bigd"])
    badsamples=names(nbigd)[which(nbigd>median(nbigd)+6*1.4826*mad(nbigd))]
    bad=bad | (ismember(Sample,badsamples) & Method=="bigd" & L<10)

    nsclip=table(Sample[Method=="sclip"])
    badsamples=names(nsclip)[which(nsclip>median(nsclip)+6*1.4826*mad(nsclip))]
    bad=bad | (ismember(Sample,badsamples) & Method=="sclip" & L<10)

    nlcd=table(Sample[Method=="lcd"])
    badsamples=names(nlcd)[which(nlcd>median(nlcd)+6*1.4826*mad(nlcd))]
    bad=bad | (ismember(Sample,badsamples) & Method=="lcd" & L<10)


}

Bad=findBad()
table(Center, Bad)  # how do the centers compare in quality?

################################################################
# Load in gene list
################################################################

adgenes=read.table("AD Gene List V2-send.txt",sep="\t",header=TRUE)
for(i in 1:ncol(adgenes)){
    if(is.factor(adgenes[,i]))adgenes[,i]=paste(adgenes[,i])
}

adgenes$GeneStart=as.numeric(adgenes$GeneStart)
adgenes$GeneEnd=as.numeric(adgenes$GeneEnd)
nrow(adgenes)
adgenes=adgenes[which(adgenes$Include=="yes"),]
nrow(adgenes)

### Create IRanges for Linkage/gene regions
adgenes.ir=GRanges(seqnames=paste(adgenes$Chr),
ranges=IRanges(start=adgenes$GeneStart,end=adgenes$GeneEnd),NearestGene=paste(adgenes$NearestGene),GeneName=paste(adgenes$GeneName),Ancestry=paste(adgenes$Ancestry))
sel=which(adgenes.ir$NearestGene=="linkage")
start(adgenes.ir[sel])=start(adgenes.ir[sel])-100000
end(adgenes.ir[sel])=end(adgenes.ir[sel])+100000

### Create IRanges for calls.
calls.ir=GRanges(seqnames=paste(Chr),ranges=IRanges(start=Start,end=End))
calls.pass.ir=GRanges(seqnames=paste(Chr[which(!Bad)]),ranges=IRanges(start=Start[which(!Bad)],end=End[which(!Bad)]))

### Count overlaps.
adgenes.fo=as.matrix(findOverlaps(adgenes.ir, calls.ir))
adgenes.co=countOverlaps(adgenes.ir, calls.ir)
adgenes.pass.co=countOverlaps(adgenes.ir, calls.pass.ir)
hist(adgenes.fo[,1], length(adgenes.ir),col="green")
table(adgenes.co)
plot(log10(width(adgenes.ir)), adgenes.co, xlab="log10 Region Size", ylab="Number of Overlaps with Calls")

HitList=vector("list",length(adgenes.co))
medianScore=rep(NA,length(adgenes.co))
nCarriers=rep(0,length(adgenes.co))
PearsonPval=rep(NA,length(adgenes.co))
FisherPval=rep(NA,length(adgenes.co))
PearsonTable=vector("list", length(adgenes.co))
FisherTable=vector("list", length(adgenes.co))
FamOfCarrier=vector("list",length(adgenes.co))
for(i in 1:length(adgenes.ir)){
    if(adgenes.co[i]>0){

        sel=which(adgenes.fo[,1]==i)
        callix=adgenes.fo[sel,2]
        nCarriers[i]=length(unique(Sample[callix]))
        mm=match(Sample[callix],famall$SampleID)
        isCarrier=rep(FALSE,nrow(famall))
        isCarrier[mm]=TRUE
        AD=famall$AD
        tt=table(AD,isCarrier)
        tt=cbind(tt,round(tt[,2]/rowSums(tt),3))
        PearsonTable[[i]]=tt
        sel=which(famall$AD<=3)
        if(sum(isCarrier[sel])==0){
            sel=c(1:nrow(famall))
        }
        pc=chisq.test(famall$AD[sel], isCarrier[sel]) #sample size different
        PearsonPval[i]=pc$p.value
        FisherTable[[i]]=matrix(c(sum(isCarrier[sel]&famall$AD[sel]>=1),
                                  sum(!isCarrier[sel]&famall$AD[sel]>=1),
                                  sum(isCarrier[sel]&famall$AD[sel]<1),
                                  sum(!isCarrier[sel]&famall$AD[sel]<1)),
                                nrow=2,
                                dimnames = list(Carrier = c("Yes", "No"),
                                                AD = c(">=1", "<1")
                                                ))
        fisher=fisher.test(FisherTable[[i]], alternative = "greater")
        FisherPval[i]=fisher$p.value
        carrierfams=famall$FamID[which(isCarrier)]
        sel=which(ismember(famall$FamID, carrierfams))
        FamOfCarrier[[i]]=cbind(famall[sel,],Carrier=isCarrier[sel])

        Fam=famall$FamID[mm]
        AD=famall$AD[mm]
        Subgroup=famall$Subgroup[mm]

        # print(adgenes[i,])
        df=data.frame(Center=Center[callix],Sample=Sample[callix],Fam=Fam,AD=AD,Subgroup=Subgroup,
                        Chr=Chr[callix],Start=Start[callix],Len=End[callix]-Start[callix],
                        Type=Type[callix],Method=Method[callix],Score=L[callix],
                        DscorePass=!Bad[callix])
        # print(df)
        HitList[[i]]=df
        medianScore[i]=median(df$Score, na.rm=TRUE)
        cat("Gene ",i,": Number of Carriers: ", nCarriers[i]," median Score: ",medianScore[i],"\n")

    } else {
        medianScore[i]=NA
    }
}

par(mfrow=c(1,1))
plot(nCarriers,medianScore, pch=18,main="Median Score Calls in Hit Regions")
grid()

par(mfrow=c(2,1))
hist(PearsonPval, 100, xlab="PearsonPvalue", ylab="PearsonPvalue in Association with AD Status",col="pink")
 plot(medianScore,pmin(10,-log(PearsonPval)), xlab="Median D-score", ylab="Pearson Association Test P-value",pch=18,main="Median
 D-score by Association")
grid()

par(mfrow=c(2,1))
hist(FisherPval, 100, xlab="FisherPvalue", ylab="FisherPvalue in Association with AD Status",col="pink")
plot(medianScore,pmin(10,-log(FisherPval)), xlab="Median D-score", ylab="Fisher Exact Test P-value",pch=18,main="Median
 D-score by Association")
grid()

#list1: cut-off D-score=10; List of D>10 hits
ord=order(medianScore,decreasing=TRUE)
nprint=sum(medianScore>10,na.rm=TRUE)

sink(paste("ADgenes","SWAN","allcenters",paste("top",nprint,sep=""),"Hits","txt",sep="."))
for(i in 1:nprint){
    cat("\n")
    write.table(adgenes[ord[i],], row.names=FALSE, sep="\t", quote=FALSE)
    cat("\n")
    sel=which(HitList[[ord[i]]]$DscorePass)
    write.table(HitList[[ord[i]]][sel,], sep="\t", row.names=FALSE, quote=FALSE)
    cat("\n")

}
sink()

#list2: cut-off PearsonP=0.0005; List of P<0.0005 hits
ord=order(-log(PearsonPval),decreasing=TRUE)
nprint=sum(PearsonPval<0.0005,na.rm=TRUE)

sink(paste("ADgenes","SWAN","allcenters","HitsByPearsonP","txt",sep="."))
for(i in 1:nprint){
    if(nCarriers[ord[i]]>=4 & !is.na(medianScore[ord[i]]) & medianScore[ord[i]]>0 ){
        cat("\n")
        cat("Chr ",adgenes$Chr[ord[i]],": ",adgenes$GeneStart[ord[i]],"-",adgenes$GeneEnd[ord[i]],"\n",sep="")
        cat("Gene Name: ",adgenes$GeneName[ord[i]],"\n")
        cat("Comment: ",adgenes$Comment[ord[i]],"\n")
        cat("Ancestry: ",adgenes$Ancestry[ord[i]],"\n")
        cat("Nearest Gene: ",adgenes$NearestGene[ord[i]],"\n")
        cat("Pearson Chisquare Pval for Association with AD Status =",PearsonPval[ord[i]],"\n")
        cat("Median D-score=",medianScore[ord[i]],", with ",nCarriers[ord[i]]," carriers\n",sep="")
        cat("Number of families: ",length(unique(FamOfCarrier[[ord[i]]]$FamID)),"\n")
        cat("\nContingency Table (AD status by Carrier status):\n")
        cat("AD Status\tNot Carrier\tCarrier\tP(Carrier)\n")
        write.table(PearsonTable[[ord[i]]],col.names=FALSE,sep="\t",quote=FALSE)
        cat("\nDetail of calls:\n")
        write.table(HitList[[ord[i]]][,1:11], sep="\t", row.names=FALSE, quote=FALSE)
    }
}
sink()

#list3: manual select 
manual=which((medianScore>10 & PearsonPval<0.0001)
            | (medianScore>4 & PearsonPval<0.0001 & nCarriers>=3 & nCarriers<15))
sink(paste("ADgenes","SWAN","allcenters","manual","txt",sep="."))
for(i in 1:nprint){
    if(nCarriers[manual[i]]>=4 & !is.na(medianScore[manual[i]]) & medianScore[ord[i]]>0 ){
        cat("\n")
        cat("Chr ",adgenes$Chr[manual[i]],": ",adgenes$GeneStart[manual[i]],"-",adgenes$GeneEnd[manual[i]],"\n",sep="")
        cat("Gene Name: ",adgenes$GeneName[manual[i]],"\n")
        cat("Comment: ",adgenes$Comment[manual[i]],"\n")
        cat("Ancestry: ",adgenes$Ancestry[manual[i]],"\n")
        cat("Nearest Gene: ",adgenes$NearestGene[manual[i]],"\n")
        cat("Pearson Chisquare Pval for Association with AD Status =",PearsonPval[manual[i]],"\n")
        cat("Median D-score=",medianScore[manual[i]],", with ",nCarriers[manual[i]]," carriers\n",sep="")
        cat("Number of families: ",length(unique(FamOfCarrier[[manual[i]]]$FamID)),"\n")
        cat("\nContingency Table (AD status by Carrier status):\n")
        cat("AD Status\tNot Carrier\tCarrier\tP(Carrier)\n")
        write.table(PearsonTable[[manual[i]]],col.names=FALSE,sep="\t",quote=FALSE)
        cat("\nDetail of calls:\n")
        sel=which(HitList[[manual[i]]]$Len<1e6)
        write.table(HitList[[manual[i]]][sel,1:11], sep="\t", row.names=FALSE, quote=FALSE)
    }
}
sink()

save.image("SWAN.allcenters.filteredcalls.overlap.RData")
load("SWAN.allcenters.filteredcalls.overlap.RData")

#charlie starts here:
#1: compare PearsonPval and FisherPval
cor(PearsonPval,FisherPval,"complete.obs") #weak 0.13 correlation

#1.2: function finds maximum frequency string
MaxTable <- function(InVec, mult = FALSE) {
  if (!is.factor(InVec)) InVec <- factor(InVec)
  A <- tabulate(InVec)
  if (isTRUE(mult)) {
    levels(InVec)[A == max(A)]
  } 
  else levels(InVec)[which.max(A)]
}

#1.5: decomposeEvents: decompose by both center and D-score
decomposeEvents=function(ListIndex,HitList,Name){ #ListIndex=3; Name="test"
  cat("Processing",ListIndex,"of",length(HitList),"Hits\n")
  if(is.null(HitList[[ListIndex]])) return(NULL)
  FullData=arrange(HitList[[ListIndex]],Score,decreasing=TRUE)
  Data=FullData[!is.na(FullData$Score) & FullData$Score>0,] #NOTE: eliminate D-score=NA and <0 entries
  Data$SubEvent=as.integer(factor(Data$Score,levels=sort(unique(Data$Score),decreasing=TRUE)))+1000000*as.integer(Data$Center) #Levels: Baylor Broad WashU
  Data$Chr=as.character(Data$Chr)
  Data$Start=as.numeric(as.character(Data$Start))
  Data$Len=as.numeric(as.character(Data$Len))
  SubVCF=mclapply(unique(Data$SubEvent),function(x){
    sel=which(Data$SubEvent==x); DScore=Data[sel,]$Score[1]; Method=MaxTable(Data[sel,]$Method)
    SubEventNum=length(sel); ID=paste(Name,ListIndex,x,sep=".");
    Ch=Data[sel,]$Chr[1]; NumSample=length(Data[sel,]$Sample); Sample=paste(Data[sel,]$Sample,collapse=",")
    St=floor(median(Data[sel,]$Start)); Size=floor(median(Data[sel,]$Len));
    StartCI=floor(mad(Data[sel,]$Start)); LenCI=floor(mad(Data[sel,]$Len)); 
    Type=MaxTable(Data[sel,]$Type);
    if(NumSample==1) return(NULL)           #NOTE: carried by only one sample, eliminate
    if(!grepl("chr",Ch)) Ch=paste('chr',Ch,sep='')
    if(Type %in% c("DEL","TRA","INV")) { #seg
      if(Size<100) Ref=genome[[Ch]][St:(St+Size)] else Ref=genome[[Ch]][St:(St+1)]
    } else { #point
      Ref=genome[[Ch]][St:(St+1)] 
    }
    if(Type %in% c("DUP")) {
      Alt=Ref
    } else if (Type %in% c("INV")) {
      Alt=reverseComplement(Ref)
    } else if (Type %in% c("INS")) {
      if(Size<100) Alt=DNAString(paste(rep("N",Size),collapse="")) else Alt="N"
    } else {
      Alt=Ref[1]
    }
    Note=paste("StartCI:",StartCI,";LenCI:",LenCI,";SubEventNum:",SubEventNum,";Method:",Method,";Sample:",Sample,sep="")
    data.frame("CHR"=Ch,"POS"=St,"ID"=ID,"REF"=toString(Ref),"ALT"=toString(Alt),"END"=St+Size,"SIZE"=Size,"TYPE"=Type,"DScore"=DScore,"PearsonP"=PearsonPval[[ListIndex]],"FisherP"=FisherPval[[ListIndex]],"N"=NumSample,"NOTE"=Note)
  })
  SubVCF=rbind.fill(SubVCF)
  #SubVCFFile=paste("ADgenes.SWAN.allcenters",Name,ListIndex,"vcf",sep=".")
  SubBEDFile=paste("ADgenes.SWAN.allcenters",Name,ListIndex,"bed",sep=".")
  #write.table(SubVCF,file=SubVCFFile,quote=FALSE,row.names=FALSE,sep="\t")
  write.table(SubVCF[,c(1,2,6,7,8,9,10,11,12,13,3)],file=SubBEDFile,quote=FALSE,row.names=FALSE,sep="\t")
  cat("Processed",ListIndex,"of",length(HitList),"Hits\n")
  return(SubVCF) 
}

#2: make a table for all hit and subevents show:
#    CH ST ED ID TYPE PP FP CADD EXON DGV NOTE
nprint=length(HitList) #nprint=7
EventVCF=rbind.fill(lapply(seq(nprint),decomposeEvents,HitList,"HitsAll"))
EventVCFFile="ADgenes.SWAN.allcenters.HitsAll.vcf"
EventBEDFile="ADgenes.SWAN.allcenters.HitsAll.bed"
write.table(EventVCF,file=EventVCFFile,quote=FALSE,row.names=FALSE,sep="\t")
write.table(EventVCF[,c(1,2,6)],file=EventBEDFile,quote=FALSE,row.names=FALSE,sep="\t")

#3: ordered by Fisher's test
ord=order(-log(FisherPval),decreasing=TRUE)
nprint=sum(FisherPval<0.05,na.rm=TRUE)
EventVCF=rbind.fill(lapply(ord[1:nprint],decomposeEvents,HitList,"HitsByFisherP"))
EventVCFFile="ADgenes.SWAN.allcenters.HitsByFisherP.vcf"
EventBEDFile="ADgenes.SWAN.allcenters.HitsByFisherP.bed"
write.table(EventVCF,file=EventVCFFile,quote=FALSE,row.names=FALSE,sep="\t")
write.table(EventVCF[,c(1,2,6)],file=EventBEDFile,quote=FALSE,row.names=FALSE,sep="\t")
sink(paste("ADgenes","SWAN","allcenters","HitsByFisherP","txt",sep="."))
for(i in 1:nprint){
  if(nCarriers[ord[i]]>=4 & !is.na(medianScore[ord[i]]) & medianScore[ord[i]]>0 ){
    cat("\n")
    cat("Chr ",adgenes$Chr[ord[i]],": ",adgenes$GeneStart[ord[i]],"-",adgenes$GeneEnd[ord[i]],"\n",sep="")
    cat("Gene Name: ",adgenes$GeneName[ord[i]],"\n")
    cat("Comment: ",adgenes$Comment[ord[i]],"\n")
    cat("Ancestry: ",adgenes$Ancestry[ord[i]],"\n")
    cat("Nearest Gene: ",adgenes$NearestGene[ord[i]],"\n")
    cat("Fisher Exact Pval for Association with AD Status =",PearsonPval[ord[i]],"\n")
    cat("Median D-score=",medianScore[ord[i]],", with ",nCarriers[ord[i]]," carriers\n",sep="")
    cat("Number of families: ",length(unique(FamOfCarrier[[ord[i]]]$FamID)),"\n")
    cat("\nContingency Table (AD status by Carrier status):\n")
    cat("AD Status\tNot Carrier\tCarrier\tP(Carrier)\n")
    write.table(PearsonTable[[ord[i]]],col.names=FALSE,sep="\t",quote=FALSE)
    cat("Decomposed Events VCF File:\n")
    cat(EventVCFFile,"\n")
    cat("Decomposed Events BED File:\n")
    cat(EventBEDFile,"\n")
    cat("\nDetail of calls:\n")
    write.table(arrange(HitList[[ord[i]]][,1:11], Score, decreasing=TRUE), sep="\t", row.names=FALSE, quote=FALSE)
  }
}
sink()

save.image("SWAN.allcenters.filteredcalls.overlap.RData")
