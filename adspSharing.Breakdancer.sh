######################################
# Only need to change these top lines
######################################

caller=Breakdancer
callershort=BD

######################################

# Baylor
center=Baylor
filename=PedigreeAnalysis/$caller.$center.raw.RData
betafile=SensMat/$caller.$center.SensMat.txt
cmd="R --no-save --args $filename $betafile  < adspSharingAll.r > $caller.$center.dscore.log 2>&1";
bsub -J $callershort$center $cmd
# WashU
center=WashU
filename=PedigreeAnalysis/$caller.$center.raw.RData
betafile=SensMat/$caller.$center.SensMat.txt
cmd="R --no-save --args $filename $betafile  < adspSharingAll.r > $caller.$center.dscore.log 2>&1";
bsub -J $callershort$center $cmd
# Broad
center=Broad
filename=PedigreeAnalysis/$caller.$center.raw.RData
betafile=SensMat/$caller.$center.SensMat.txt
cmd="R --no-save --args $filename $betafile  < adspSharingAll.r > $caller.$center.sharing.log 2>&1";
bsub -J $callershort$center $cmd


