######################################
# Only need to change these top lines
######################################

caller=Lumpy
callershort=Lum

######################################

# Baylor
center=Baylor
filename=PedigreeAnalysis/$caller/$caller.$center.sharing.RData
betafile=SensMat/$caller.$center.SensMat.txt
cmd="R --no-save --args $filename $betafile  < adspSharingAll.r > $caller.$center.sharing.log 2>&1";
bsub -J $callershort$center $cmd
# WashU
center=WashU
filename=PedigreeAnalysis/$caller/$caller.$center.sharing.RData
betafile=SensMat/$caller.$center.SensMat.txt
cmd="R --no-save --args $filename $betafile  < adspSharingAll.r > $caller.$center.sharing.log 2>&1";
bsub -J $callershort$center $cmd
# Broad
center=Broad
filename=PedigreeAnalysis/$caller/$caller.$center.sharing.RData
betafile=SensMat/$caller.$center.SensMat.txt
cmd="R --no-save --args $filename $betafile  < adspSharingAll.r > $caller.$center.sharing.log 2>&1";
bsub -J $callershort$center $cmd
