######################################
# Only need to change these top lines
######################################

caller=Pindel
callershort=PD

######################################

# Baylor
center=Baylor
filename=PedigreeAnalysis/$caller/$caller.$center.sharing.RData
betafile=SensMat/$caller.$center.SensMat.txt
cmd="R --no-save --args $filename $betafile  < adspSharingAll.debug.PDBa.r > debug.PDBa.log 2>&1";
bsub -J $callershort$center $cmd
# WashU
center=WashU
filename=PedigreeAnalysis/$caller/$caller.$center.sharing.RData
betafile=SensMat/$caller.$center.SensMat.txt
cmd="R --no-save --args $filename $betafile  < adspSharingAll.debug.PDWa.r > debug.PDWa.log 2>&1";
bsub -J $callershort$center $cmd
# Broad
center=Broad
filename=PedigreeAnalysis/$caller/$caller.$center.sharing.RData
betafile=SensMat/$caller.$center.SensMat.txt
cmd="R --no-save --args $filename $betafile  < adspSharingAll.debug.PDBr.r > debug.PDBr.log 2>&1";
bsub -J $callershort$center $cmd


