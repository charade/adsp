######################################
# Only need to change these top lines
######################################

caller=SWAN
callershort=SWc

######################################

# Baylor
#center=Baylor
#filename=PedigreeAnalysis/$caller/$caller.$center.sharing.clean.RData
#betafile=SensMat/$caller.$center.SensMat.txt
#cmd="R --no-save --args $filename $betafile  < adspSharingAll.r > $caller.$center.sharing.clean.log 2>&1";
#bsub -J $callershort$center $cmd
# WashU
center=WashU
filename=PedigreeAnalysis/$caller/$caller.$center.sharing.cleaned.RData
betafile=SensMat/$caller.$center.SensMat.txt
cmd="R --no-save --args $filename $betafile  < adspSharingAll.r > $caller.$center.sharing.clean.log 2>&1";
bsub -J $callershort$center $cmd
# Broad
#center=Broad
#filename=PedigreeAnalysis/$caller/$caller.$center.sharing.clean.RData
#betafile=SensMat/$caller.$center.SensMat.txt
#cmd="R --no-save --args $filename $betafile  < adspSharingAll.r > $caller.$center.sharing.clean.log 2>&1";
#bsub -J $callershort$center $cmd


