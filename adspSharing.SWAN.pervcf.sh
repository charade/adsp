######################################
# Only need to change these top lines
######################################

caller=SWAN
callershort=SW
betafile=SensMat.txt

######################################

# WashU
center=WashU
filename=PedigreeAnalysis/$caller/$caller.$center.sharing.cleaned.RData

for vid in $(seq 1 186);
do
	cmd="R --no-save --args $filename $betafile $vid  < adspSharingAllperVCF.r > $caller.$center.$vid.sharing.log 2>&1";
	bsub -J $callershort$vid $cmd
done


