famfile=ADSP_WGSfam.txt


######################
# Lumpy
######################
caller=Lumpy
callershort=Lum
# Baylor
center=Baylor
betafile="SensMat.txt"
filename=PedigreeAnalysis/$caller/$caller.$center.RData
cmd="R --no-save --args $center $filename $betafile < adspSharingMakeMasterRData.r > $caller.$center.makemaster.log 2>&1";
bsub -J $callershort$center $cmd
# WashU
center=WashU
betafile="SensMat.txt"
filename=PedigreeAnalysis/$caller/$caller.$center.RData
cmd="R --no-save --args $center $filename $betafile < adspSharingMakeMasterRData.r > $caller.$center.makemaster.log 2>&1";
bsub -J $callershort$center $cmd
# Broad
center=Broad
betafile="SensMat.txt"
filename=PedigreeAnalysis/$caller/$caller.$center.RData
cmd="R --no-save --args $center $filename $betafile < adspSharingMakeMasterRData.r > $caller.$center.makemaster.log 2>&1";
bsub -J $callershort$center $cmd

