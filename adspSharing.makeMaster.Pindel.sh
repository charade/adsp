
######################
# Pindel
######################
caller=Pindel   
callershort=PD

##### Only need to change the above two lines.

# Baylor
center=Baylor
filename=PedigreeAnalysis/$caller/$caller.$center.RData
cmd="R --no-save --args $center $filename < adspSharingMakeMasterRData.r > $caller.$center.makemaster.log 2>&1";
bsub -J $callershort$center $cmd
# WashU
center=WashU
filename=PedigreeAnalysis/$caller/$caller.$center.RData
cmd="R --no-save --args $center $filename < adspSharingMakeMasterRData.r > $caller.$center.makemaster.log 2>&1";
bsub -J $callershort$center $cmd
# Broad
center=Broad
filename=PedigreeAnalysis/$caller/$caller.$center.RData
cmd="R --no-save --args $center $filename < adspSharingMakeMasterRData.r > $caller.$center.makemaster.log 2>&1";
bsub -J $callershort$center $cmd

