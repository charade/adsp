famfile=ADSP_WGSfam.txt


######################
# SWAN
######################
caller=SWAN
callershort=SW

# Baylor
center=Baylor
centerdir=/home/xiac/baylor_rj
cmd="R --no-save --args $center $centerdir $famfile < convertSWAN.r > convertSWAN.$center.log 2>&1";
bsub -J $callershort$center $cmd

# WashU
center=WashU
centerdir=/home/xiac/washu_rj/
cmd="R --no-save --args $center $centerdir $famfile < convertSWAN.r > convertSWAN.$center.log 2>&1";
bsub -J $callershort$center $cmd



# Broad
center=Broad
centerdir=../../xiac/broad_rj
cmd="R --no-save --args $center $centerdir $famfile < convertSWAN.r > convertSWAN.$center.log 2>&1";
#####bsub -J $callershort$center $cmd

