#!/home/xiac/usr/bin/Rscript

debug=TRUE
cat("debug=", debug,"\n")

args = commandArgs(trailingOnly=TRUE)
print(args)
filename = args[1]      # name of RData file containing vcf, fam table.
betafile = args[2]      # filename containing the sensitivities

if(debug){
  setwd("~/adsp_dscore")
  #betafile="SensMat/SWAN.WashU.SensMat.txt"
  #filename="PedigreeAnalysis/SWAN/SWAN.WashU.sharing.RData"
  #Breakdancer.Broad.sharing.log
  betafile="SensMat/Breakdancer.Broad.SensMat.txt"
  filename="PedigreeAnalysis/Breakdancer/Breakdancer.Broad.sharing.RData"
}

library(GenomicRanges)


#####################################################
# Get Sensmat.
#####################################################
sensmat=read.table(betafile,sep="\t",header=TRUE)


########################################################
# Load file, make sure that start is smaller than end.
########################################################

load(filename)
source("adsplib.r")
outprefix= unlist(strsplit(filename,".RData"))
outfile=paste(outprefix, ".post.RData",sep="")

for(vcfind in 1:length(vcf)){
    st=vcf[[vcfind]]$START
    ed=vcf[[vcfind]]$END
    vcf[[vcfind]]$START=pmin(st,ed)
    vcf[[vcfind]]$END=pmax(st,ed)
}

########################################################
# Run sharing.
########################################################


for(vcfind in 1:length(vcf)){
    #if(debug) vcfind=188
    cat("\nDoing vcf number ",vcfind,": ",nrow(vcf[[vcfind]])," entrees.\n",sep="")
    ptm=proc.time()
    sharing=getFamilyVSPopOverlap2(vcfind,vcf,fam,sensmat)
    #tryCatch(
    #sharing=getFamilyVSPopOverlap2(vcfind,vcf,fam,sensmat),
    #	finally = { sharing=NULL }
    #	)
    vcf[[vcfind]]=cbind(vcf[[vcfind]],sharing)
    save.image(outfile)
    elapsed=proc.time()-ptm
    cat("VCF ",vcfind, " took ",elapsed[3]," seconds.\n",sep="")
    #if(debug) break
}
