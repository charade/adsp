#!/home/xiac/usr/bin/Rscript

args = commandArgs(trailingOnly=TRUE)
print(args)
filename = args[1]      # name of RData file containing vcf, fam table.
betafile = args[2]      # filename containing the sensitivities
debug = args[3]

if(!is.na(debug)) debug=TRUE else debug=FALSE
cat("debug=", debug,"\n")

library(GenomicRanges)

if(debug){
  setwd("~/adsp_dscore")
  betafile="SensMat/Pindel.Broad.SensMat.txt"
  filename="PedigreeAnalysis/Pindel.Broad.raw.RData"
  #debugidx=69
  #betafile="SensMat/Delly.WashU.SensMat.txt"
  #filename="PedigreeAnalysis/Delly.WashU.raw.RData"
}

#####################################################
# Get Sensmat.
#####################################################
sensmat=read.table(betafile,sep="\t",header=TRUE)


########################################################
# Load file, make sure that start is smaller than end.
########################################################

load(filename)
source("adsplib.r")
outprefix= unlist(strsplit(filename,".RData"))
outfile=paste(outprefix, ".post.RData",sep="")

cat("Verifying VCF formats for ", length(vcf), " samples\n")

for(vcfind in seq_along(vcf)){
    cat("vcfind=",vcfind,"\n")
    #print(vcf[[vcfind]])
    if(nrow(vcf[[vcfind]])>0) { #this handles zero entry data frame
      st=vcf[[vcfind]]$START
      ed=vcf[[vcfind]]$END
      vcf[[vcfind]]$START=pmin(st,ed)
      vcf[[vcfind]]$END=pmax(st,ed)
    }
}

########################################################
# Run sharing.
########################################################

cat("Computing D-score for ", length(vcf), " samples\n")
for(vcfind in seq_along(vcf)){
    if(debug && (vcfind != debugidx)) next
    cat("\nDoing vcf number ",vcfind," of ", length(vcf),
	  " of ",nrow(vcf[[vcfind]])," entries.\n",sep="")
    if(nrow(vcf[[vcfind]])>0) {
      ptm=proc.time()
      sharing=getFamilyVSPopOverlap2(vcfind,vcf,fam,sensmat)
      vcf[[vcfind]]=cbind(vcf[[vcfind]],sharing)
      elapsed=proc.time()-ptm
    }
    cat("VCF ",vcfind, " took ",elapsed[3]," seconds.\n",sep="")
}
save.image(outfile)

cat("adspDscore-DONE\n")
