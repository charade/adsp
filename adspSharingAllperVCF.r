
args = commandArgs(trailingOnly=TRUE)
print(args)
filename = args[1]      # name of RData file containing vcf, fam table.
betafile = args[2]      # filename containing the sensitivities
vid=as.numeric(args[3])

source("adsplib.r")
library(GenomicRanges)


#####################################################
# Get Sensmat.
#####################################################
sensmat=read.table(betafile,sep="\t",header=TRUE)


########################################################
# Load file, make sure that start is smaller than end.
########################################################

load(filename)
outprefix= unlist(strsplit(filename,".RData"))
outfile=paste(outprefix, ".",vid,".post.RData",sep="")

for(vcfind in 1:length(vcf)){
    st=vcf[[vcfind]]$START
    ed=vcf[[vcfind]]$END
    vcf[[vcfind]]$START=pmin(st,ed)
    vcf[[vcfind]]$END=pmax(st,ed)
}

########################################################
# Run sharing.
########################################################

for(vcfind in 1:length(vcf)){

    vcfind=vid

    cat("\nDoing vcf number ",vcfind,": ",nrow(vcf[[vcfind]])," entrees.\n",sep="")
    ptm=proc.time()
    sharing=getFamilyVSPopOverlap2(vcfind,vcf,fam,sensmat)
    vcf[[vcfind]]=cbind(vcf[[vcfind]],sharing)
    save.image(outfile)
    elapsed=proc.time()-ptm
    cat("VCF ",vcfind, " took ",elapsed[3]," seconds.\n",sep="")

}

