
args = commandArgs(trailingOnly=TRUE)
print(args)
center = args[1]       # center where the sequencing was done
filename = args[2]      # name of RData file 
famfile="ADSP_WGSfam.txt"

# filename="PedigreeAnalysis/Lumpy/Lumpy.baylor.RData"
# famfile="ADSP_WGSfam.txt"


source("adsplib.r")
library(GenomicRanges)

outprefix= unlist(strsplit(filename,".RData"))
outprefix=paste(outprefix,"sharing",sep=".")
outfile=paste(outprefix, "RData",sep=".")

#####################################################
# Get family file, get vcfs
#####################################################
fam=read.table(famfile,header=TRUE,sep="\t",stringsAsFactors=FALSE)
throw=which(fam$SeqCtr!=center)
fam=fam[-throw,]

load(filename)
SampleID=getElementFromListOfLists(samplelist, 2)
mm=match(SampleID,fam$SampleID)
throw.vcf=which(is.na(mm))
if(length(throw.vcf)>0){
    vcf=vcf[-throw.vcf]
    SampleID=SampleID[-throw.vcf]
    mm=mm[-throw.vcf]
}
fam=fam[mm,]

save.image(outfile)
