
library(IRanges)
source("adsplib.r")
callers=c("Breakdancer","CNVnator","Delly","Lumpy","SWAN")
centers=c("Baylor","Broad","WashU")


###################################################################
# Load in sample IDs of all callers over all centers,
# harmonize by sampleID.
###################################################################

sampleIDs=as.list(numeric(length(centers)*length(callers)))
dim(sampleIDs)=c(length(centers),length(callers))

for(centi in 1:length(centers)){
    for(calli in 1:length(callers)){
        caller=callers[calli]
        center=centers[centi]
        cat("Loading center ",center," for caller ", caller,"\n", sep="")
        filename=file.path("PedigreeAnalysis",caller,paste(caller,center,"post","RData",sep="."))
        tryload=try(load(filename))
        if(class(tryload)=="try-error"){
            cat("Can not find file: ",filename,"\n")
            sampleIDs[[centi,calli]]=list()
        } else {
            cat("There are ",nrow(fam)," samples.\n",sep="")
            sampleIDs[[centi, calli]]=fam$SampleID      # use fam, not sampleIDs, because SWAN doesn't have it.
        }
    }
}

sampleIDs.master=vector("list",length(centers))
for(centi in 1:length(centers)){
    sampleIDs.master[[centi]]=sampleIDs[[centi,1]] 
    for(calli in 2:length(callers)){
        sampleIDs.master[[centi]]=intersect(sampleIDs.master[[centi]], sampleIDs[[centi,calli]]) # get intersection
    }
}

### Harmonize by sampleID
for(centi in 1:length(centers)){
    for(calli in 1:length(callers)){
        caller=callers[calli]
        center=centers[centi]
        cat("Doing ",callers[calli],centers[centi],"\n")
        filename=file.path("PedigreeAnalysis",caller,paste(caller,center,"post","RData",sep="."))
        tryload=try(load(filename))
        if(class(tryload)=="try-error"){
            cat("Can not find file: ",filename,"\n")
        } else {
            cat("Harmonizing ...\n")
            mm=match(sampleIDs.master[[centi]], sampleIDs[[centi, calli]])
            vcf=vcf[mm]; fam=fam[mm,]
            
            for(si in 1:length(vcf)){
                if(is.null(vcf[[si]])){
                    vcf[[si]]=matrix(nrow=0,ncol=ncol(vcf[[1]]))
                } else {
                    sstats=vcf[[si]]
                    st=pmin(sstats$START, sstats$END)
                    ed=pmax(sstats$START, sstats$END)
                    vcf[[si]]$START=st; vcf[[si]]$END=ed;
                }
            }
            save(vcf, fam, file=file.path("PedigreeAnalysis",caller,paste(caller,center,"harmonized","RData",sep=".")))
        }

    }
}



###################################################################
# Compute overlaps
# For every caller and center, CallerOverlap is a list having the
# same length as vcf, i.e. number of samples, with columns
# Overlap.Caller and UniqueCall
# 
# Also aggregate across samples and save annotations in vectors, and
# saved individually as RData file for quick read and write.
###################################################################
for(centi in 1:length(centers)){
    
    for(calli in 1:length(callers)){
        caller=callers[calli]
        center=centers[centi]
        filename=file.path("PedigreeAnalysis",caller,paste(caller,center,"harmonized","RData",sep="."))
        cat("Loading ",paste(filename),"\n")
        load(filename)
        vcf1=vcf
        fam1=fam
        
        CO=vector("list",length=length(vcf1))
        for(si in 1:length(vcf1)){ 
            if(!is.null(nrow(vcf1[[si]]))) CO[[si]]=matrix(nrow=nrow(vcf1[[si]]),ncol=length(callers), data=0)
        }
        
        ### Loop over callers to compute overlap.
        for(calli2 in 1:length(callers)){
            if(calli2==calli){
                for(i in 1:length(vcf1)) CO[[si]][,calli2]=rep(1,nrow(vcf1[[si]]))
            } else {
                caller2=callers[calli2]
                filename2=file.path("PedigreeAnalysis",caller2,paste(caller2,center,"harmonized","RData",sep="."))
                cat("Loading ",paste(filename2),"\n")
                load(filename2)
                vcf2=vcf
                for(si in 1:length(vcf1)){
                    cat("Comparing ",caller, " to ", caller2," for center ",centers[centi],", ", "si=",si,"\n",sep="")    
                    sstats=vcf1[[si]]
                    sstats2=vcf2[[si]]
                    if(nrow(sstats)>0 & nrow(sstats2)>0){
                        ir=IRanges(start=sstats$START,end=sstats$END)
                        ir2=IRanges(start=sstats2$START, end=sstats2$END)
                        co=hasOverlap(ir,ir2,MAX.OFFSET=200)   
                        CO[[si]][,calli2]=co
                    }
                }
            }
        }
        
        cat("Computing UniqueCall ")
        ### Create UniqueCall and add it to CO data frame.
        for(si in 1:length(vcf1)){  
            cat(".")                          
            UniqueCall=rowSums(CO[[si]][,-calli])==0
            CO[[si]]=as.data.frame(cbind(CO[[si]],UniqueCall))
            names(CO[[si]])=c(paste("Overlap.",callers,sep=""),"UniqueCall")
        }
        cat("\n")
        
        ### Aggregate across samples.
        cat("Aggregating across samples ")
        Chrom=SampleID=Type=rep("",0)
        SwanOverlap=popFreq=sibShare=Start=Len=Score=Unique=rep(0,0)
        if(calli==which(callers=="SWAN")) METHOD=rep("",0)
        for(si in 1:length(vcf1)){
            cat("si=",si,"\n")
            if(nrow(vcf1[[si]])>0){
                Chrom=c(Chrom,paste(vcf1[[si]]$CHR))
                Start=c(Start,vcf1[[si]]$START)
                Len=c(Len,vcf1[[si]]$END-vcf1[[si]]$START)
                Score=c(Score,vcf1[[si]]$L)
                sibShare=c(sibShare,vcf1[[si]]$sibshare)
                Unique=c(Unique,CO[[si]]$UniqueCall)
                Type=c(Type,paste(vcf1[[si]]$TYPE))
                popFreq=c(popFreq,vcf1[[si]]$overallFreq)
                SwanOverlap=c(SwanOverlap,CO[[si]]$Overlap.SWAN)
                SampleID=c(SampleID, rep(fam$SampleID[si],nrow(vcf1[[si]])))
                #if(calli==which(callers=="SWAN")){
                #    METHOD=c(METHOD,paste(vcf1[[si]]$METHOD))
                #}
            }
        }
        cat("\n")
        vcf=vcf1
        CallerOverlap=CO
        save(vcf, fam, CallerOverlap, Chrom, SampleID, Type, SwanOverlap, popFreq, sibShare, Start, Len, Score, Unique, file=filename)
        
        # Save each individually so that they load faster.
        save(Type, file=file.path("PedigreeAnalysis",caller,paste(caller,center,"harmonized","Type","RData",sep=".")))
        save(Len, file=file.path("PedigreeAnalysis",caller,paste(caller,center,"harmonized","Len","RData",sep=".")))
        save(Unique, file=file.path("PedigreeAnalysis",caller,paste(caller,center,"harmonized","Unique","RData",sep=".")))
        save(Chrom, file=file.path("PedigreeAnalysis",caller,paste(caller,center,"harmonized","Chrom","RData",sep=".")))
        save(Start, file=file.path("PedigreeAnalysis",caller,paste(caller,center,"harmonized","Start","RData",sep=".")))
        save(popFreq, file=file.path("PedigreeAnalysis",caller,paste(caller,center,"harmonized","popFreq","RData",sep=".")))
        save(sibShare, file=file.path("PedigreeAnalysis",caller,paste(caller,center,"harmonized","sibShare","RData",sep=".")))
        save(SampleID, file=file.path("PedigreeAnalysis",caller,paste(caller,center,"harmonized","SampleID","RData",sep=".")))
        save(Score, file=file.path("PedigreeAnalysis",caller,paste(caller,center,"harmonized","Score","RData",sep=".")))
        
    }
}

################################################################################################################################
## The code below requires the Caller.Center.harmonized.*.RData files.
################################################################################################################################
#setwd("C:\\Users\\nzh.WHARTON\\Dropbox\\StructuralVariation\\ADSP\\")
#source("adsplib.r")
#centers=c("Baylor","Broad")
#callers=c("Breakdancer","CNVnator","Delly","Lumpy","SWAN")
#
#
#
####################################################################
## Table of event types and scores for each caller.
####################################################################
#par(mfrow=c(length(centers),length(callers)))
#breaks=seq(2000,20000,2000)
#
#for(centi in 1:length(centers)){
#    for(calli in 1:length(callers)){
#        caller=callers[calli]
#        center=centers[centi]
#        load(file=file.path("PedigreeAnalysis",caller,paste(caller,center,"harmonized","Type","RData",sep=".")))
#        load(file=file.path("PedigreeAnalysis",caller,paste(caller,center,"harmonized","SampleID","RData",sep=".")))
#        t=table(SampleID)
#        hist(threshold(t,2000,20000),breaks=breaks,col="skyblue",main=paste(caller,center),xlab="Number of Calls per Sample")
#        cat("\n",center, " ",caller,":\n",sep="")
#        print(round(table(Type)/length(unique(SampleID))))
#    }
#}
#
#
#
####################################################################
## Deletion Lengths
####################################################################
#par(mfrow=c(length(centers),length(callers)))
#breaks=seq(0,2000,100)
#
#for(centi in 1:length(centers)){
#    for(calli in 1:length(callers)){
#        caller=callers[calli]
#        center=centers[centi]
#        load(file=file.path("PedigreeAnalysis",caller,paste(caller,center,"harmonized","Type","RData",sep=".")))
#        load(file=file.path("PedigreeAnalysis",caller,paste(caller,center,"harmonized","Len","RData",sep=".")))
#        load(file=file.path("PedigreeAnalysis",caller,paste(caller,center,"harmonized","Unique","RData",sep=".")))
#        mainstr=paste(caller,center,"DEL",sep=" ")
#        sel=which(Type=="DEL")
#        h=shadedHistogram(Len[sel], Unique[sel], horizontal=TRUE, 
#                breaks=breaks,shadeColor="pink",nonshadeColor="gainsboro",label="Length",
#                main=mainstr,cex.main=1.5, cex.lab=1.5)
#    }
#}
#
####################################################################
## Shaded histogram breakdown by center, caller:
##       population Frequency.
####################################################################
#par(mfrow=c(length(centers),length(callers)))
#breaks=seq(0,1,0.05)
#for(centi in 1:length(centers)){
#    for(calli in 1:length(callers)){
#        caller=callers[calli]
#        center=centers[centi]
#        load(file=file.path("PedigreeAnalysis",caller,paste(caller,center,"harmonized","popFreq","RData",sep=".")))
#        load(file=file.path("PedigreeAnalysis",caller,paste(caller,center,"harmonized","Unique","RData",sep=".")))
#        
#        mainstr=paste(caller,center,sep=" ")
#        h=shadedHistogram(popFreq, Unique, horizontal=TRUE, 
#                breaks=breaks,shadeColor="pink",nonshadeColor="gainsboro",label="Call Frequency",
#                main=mainstr,cex.main=1.5, cex.lab=1.5)
#
#    }
#}
#
#
####################################################################
## Overlap with AD genes list
####################################################################
