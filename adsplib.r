

####################
# Helper functions
####################


readVCF<-function(filename){
    tab=read.table(filename,header=FALSE,sep="\t")
    temp=strsplit(paste(tab$V4),split=".",fixed=TRUE)
    method=rep("",nrow(tab)); 
    for(i in 1:length(temp)){
        method[i]=temp[[i]][length(temp[[i]])]
        if(substr(method[i],1,5)=="sclip") method[i]="sclip"
    }
    svfreq=rep(NA,nrow(tab))
    totreads=rep(NA,nrow(tab))
    for(i in 1:nrow(tab)){
        temp=strsplit(paste(tab$V9[i]),":",fixed=TRUE)
        temp=strsplit(temp[[1]][length(temp[[1]])],";",fixed=TRUE)
        temp=strsplit(temp[[1]],"=",fixed=TRUE)
        for(j in 1:length(temp)){
            if(temp[[j]][1]=="AA") svfreq[i] = as.numeric(temp[[j]][2])
            if(temp[[j]][1]=="DP") totreads[i] = as.numeric(temp[[j]][2])
        }
    }
    calls=data.frame(CHR=tab$V1,START=tab$V2,END=tab$V3,TYPE=tab$V6,METHOD=method,PURITY=svfreq,TOTREADS=totreads)
    calls
}


overlapMatrix<-function(vcf, LEN.RANGE=c(0,Inf),TYPE="ALL",METHOD="ALL",excludeXY=TRUE,MAX.LEN=100000){
    cat("Constructing overlap matrix with TYPE=",TYPE,", METHOD=",METHOD, ", excludeXY=",excludeXY,", LEN.RANGE=",LEN.RANGE,"\n",sep="")
    irs=vector("list",length(vcf))
    irsAll=vector("list",length(vcf))
    for(i in 1:length(vcf)){
        tvcf=vcf[[i]]
        tvcf$CHR=paste(tvcf$CHR)
        if(excludeXY){
            throw=tvcf$CHR=="X" | tvcf$CHR=="Y" | tvcf$CHR=="23"
            tvcf=tvcf[!throw,]
        } 
        
        if(length(tvcf)>0){
            st=pmin(tvcf$START,tvcf$END)
            ed=pmax(tvcf$START,tvcf$END)
            tvcf$START=st
            tvcf$END=ed
            len=tvcf$END-tvcf$START
            matchType=rep(TRUE,length(len))
            matchMethod=rep(TRUE,length(len))
            if(TYPE != "ALL") matchType=(tvcf$TYPE==TYPE)
            if(METHOD!="ALL") matchMethod=(tvcf$METHOD==METHOD)
            sel=which(len>LEN.RANGE[1] & len<LEN.RANGE[2] & matchType & matchMethod)
            
            irs[[i]]=GRanges(seqnames=tvcf$CHR[sel],ranges=IRanges(start=tvcf$START[sel]-500,end=tvcf$END[sel]+500))
            sel=which(len<MAX.LEN)  
            irsAll[[i]]=GRanges(seqnames=tvcf$CHR[sel],ranges=IRanges(start=tvcf$START[sel]-500,end=tvcf$END[sel]+500))
        } else {
            irs[[i]]=GRanges()
         #  irs[[i]]=IRanges() 
           irsAll[[i]]=GRanges() 
        }
    }
    cat("Finished constructing Genomic Ranges.\n")
    overlaps=matrix(nrow=length(vcf), ncol=length(vcf))
    for(i in 1:length(vcf)){
        cat("i=",i,": ")
        for(j in 1:length(vcf)){
#            cat("i=",i,",j=",j,"\n")
            cat(".")
            overlaps[i,j]=sum(countOverlaps(irs[[i]],irsAll[[j]])>0)
        }
        cat("done.\n")
    }
    overlaps
}

computeDscores<-function(ov,trios,doplot=FALSE){
    dscore=rep(0,nrow(trios))
    for(i in 1:nrow(trios)){
        if(i %% 10000 == 0 ) cat(i," out of ",nrow(trios)," done.\n",sep="")
        dscore[i]=(ov[trios[i,1],trios[i,2]]-ov[trios[i,1],trios[i,3]])/ov[trios[i,1],trios[i,1]]
    }
    if(doplot) hist(dscore,col="cornflowerblue",100)
    list(dscores=dscore,dscore.mean=mean(dscore))
}

getDscoreTrios<-function(sampleNames,fam){
    mm=match(sampleNames,fam$SampleID)
    cat(sum(!is.na(mm)),"samples are found in family file.\n")
    fam=fam[mm,]

    samecenter=matrix(nrow=nrow(fam),ncol=nrow(fam),data=0)
    for(i in 1:nrow(fam)){
        sel=which(fam$SeqCtr == fam$SeqCtr[i])
        samecenter[i,sel]=1
    }


    fammat=matrix(nrow=nrow(fam),ncol=nrow(fam),data=0)
    ## 0=unrelated, 1=sib, 2=parent-child, 3=identity
    
    for(i in 1:nrow(fam)){
        sel=which(fam$FamID != fam$FamID[i])
        fammat[i,sel]=0
        sel=which(fam$Father==fam$Father[i] & fam$Mother==fam$Mother[i])
        fammat[i,sel]=1
        sel=which(fam$Father==fam$SUBJID[i] | fam$Mother==fam$SUBJID[i])
        fammat[i,sel]=2
        fammat[sel,i]=2
        fammat[i,i]=3
    }

    trios=data.frame(matrix(nrow=0,ncol=3))
    cat("Finding trios ...\n")
    for(i in 1:nrow(fam)){
        if(i %% 20==0) cat(i,"\n")
        sel=which((fammat[i,]==1 | fammat[i,]==2))
        if(length(sel)>0){
            outg=which(fammat[i,]==0)
            temp=cbind(i,rep(sel,length(outg)),rep(outg,each=length(sel)))
            trios=rbind(trios,temp)
        }
    }
    names(trios)=c("Center","Related","Outgroup")
    trios
}

summarizeVCFs<-function(vcf,sampleNames,prefix="Broad"){
    
    nTOTAL=rep(0,length(vcf))
    nDEL=rep(0,length(vcf))
    nINS=rep(0,length(vcf))
    nTRP=rep(0,length(vcf))
    nINV=rep(0,length(vcf))
    nTANDUP=rep(0,length(vcf))
    
    dels=GRanges()
    tandups=GRanges()
    insertions=GRanges()
    for(i in 1:length(vcf)){
        cat("Processing vcf ",i," out of ",length(vcf),"... \n",sep="")
        if(length(vcf[[i]])!=0){
            nTOTAL[i]=nrow(vcf[[i]])
            nDEL[i] = sum(vcf[[i]]$TYPE=="DEL",na.rm=TRUE)
            nINS[i] = sum(vcf[[i]]$TYPE=="INS" | vcf[[i]]$TYPE=="DOM.INS",na.rm=TRUE)
            nTRP[i] = sum(vcf[[i]]$TYPE=="TRP",na.rm=TRUE)
            nINV[i] = sum(vcf[[i]]$TYPE=="INV",na.rm=TRUE)
            nTANDUP[i] = sum(vcf[[i]]$TYPE=="TANDEM.DUP",na.rm=TRUE)
            sel=which(vcf[[i]]$TYPE=="DEL")
            if(length(sel)!=0) dels=c(dels,GRanges(seqnames=vcf[[i]]$CHR[sel],ranges=IRanges(start=vcf[[i]]$START[sel],end=vcf[[i]]$END[sel])))
            sel=which(vcf[[i]]$TYPE=="TANDEM.DUP")
            if(length(sel)!=0) tandups=c(tandups,GRanges(seqnames=vcf[[i]]$CHR[sel],ranges=IRanges(start=vcf[[i]]$START[sel],end=vcf[[i]]$END[sel])))
            sel=which(vcf[[i]]$TYPE=="INS")
            if(length(sel)!=0) insertions=c(insertions,GRanges(seqnames=vcf[[i]]$CHR[sel],ranges=IRanges(start=vcf[[i]]$START[sel],end=vcf[[i]]$END[sel])))

        }
    }
    sumtab=data.frame(sampleNames,nTOTAL,nDEL,nINS,nTRP,nINV,nTANDUP)
    
    png(paste(prefix,"_Totals.png",sep=""),height=600,width=900)
    par(mfrow=c(2,3))
    hist(nTOTAL,30,col="cornflowerblue")
    hist(nDEL,30,col="cornflowerblue")
    hist(nINS,30,col="cornflowerblue")
    hist(nTRP,30,col="cornflowerblue")
    hist(nINV,10,col="cornflowerblue")
    hist(nTANDUP,30,col="cornflowerblue")
    dev.off()
    
    png(paste(prefix,"_Pie.png",sep=""),height=600,width=600)
    slices=c(sum(nDEL),sum(nINS),sum(nTRP),sum(nINV),sum(nTANDUP))
    lbs=c("DEL","INS","TRP","INV","TANDUP")
    pie(slices,labels=lbs,main="Distribution of Called Event Types")
    dev.off()

    png(paste(prefix,"_width_del.png",sep=""),height=400,width=600)    
    wi=width(dels)
    hist(pmin(wi,1000),100,col="cornflowerblue",main="Width of DEL",xlab="Bp")
    dev.off()
    
}


#### This is a depreciated function, see hasOverlap
overlapBreakpoints<-function(ir1,ir2,maxdiff=100){
    st1=IRanges(start=start(ir1)-maxdiff, end=start(ir1)+maxdiff)
    st2=IRanges(start=start(ir2),width=1)
    ovSt=findOverlaps(st1,st2)
    ed1=IRanges(start=end(ir1)-maxdiff, end=end(ir1)+maxdiff)
    ed2=IRanges(start=end(ir2),width=1)
    ovEd=findOverlaps(ed1,ed2)
    
    inter=intersect(ovSt,ovEd)
    temp=as.matrix(inter)
    ovBoth=rep(FALSE,length(ir1))
    ovBoth[unique(temp[,1])]=TRUE

    
    tab=table(temp[,1])   
}

testPrint<-function(){
  print("printed\n")
  cat("catted\n")
}

getFamilyVSPopOverlap2<-function(vcfind,vcf,fam,sensmat=NULL){
    #sink("DEBUG.txt", append=FALSE, split=FALSE)
    ovmat=matrix(nrow=nrow(vcf[[vcfind]]),ncol=length(vcf),data=0)
    # ovmat here is specific to vcf[[vcfind]], has dimensions (num calls in vcf) x (num vcfs)
    chrs=unique(vcf[[vcfind]]$CHR)
    for(ch in chrs[!is.na(chrs)]){ #avoid CHR==NA
        cat("\tNow processing Chr ", ch, ": ",sep="")
        ptm=proc.time()
        sel=which(vcf[[vcfind]]$CHR==ch)
        ir1=IRanges(start=vcf[[vcfind]]$START[sel],end=vcf[[vcfind]]$END[sel])
        for(i in 1:length(vcf)){
#            cat(".")
            sel2=which(vcf[[i]]$CHR==ch)
            ir2=IRanges(start=vcf[[i]]$START[sel2],end=vcf[[i]]$END[sel2])
            ovmat[sel,i]=hasOverlap(ir1,ir2,MAX.OFFSET=200)
        }
        elapsed=proc.time()-ptm
        cat("That took ",elapsed[3]," seconds.\n",sep="")
    }
    
    TYPE=paste(vcf[[vcfind]]$TYPE)
    LEN=abs(vcf[[vcfind]]$END-vcf[[vcfind]]$START)
    
    sibsets=findSibs(fam)
    famsets=findFams(fam)
    overallCount=rowSums(ovmat)  
    overallFreq=overallCount/ncol(ovmat)
    res=lapply(seq(nrow(ovmat)),FUN=function(x){ pSharing(ovmat[x,],sibsets) })
    #res=apply(ovmat,1,pSharing,sets=sibsets)  #this breaks the program for some reason unknown.
    sibshare=rep(0,length(res)); sibcount=rep(0,length(res))
    for(i in 1:length(res)){ 
        sibshare[i]=res[[i]]$pshare
        sibcount[i]=res[[i]]$denom
    }
    
    p1sib=rep(NA,length(sibshare))  # p1 is the "p-value" assuming true.
    p0sib=rep(NA,length(sibshare))  # p0 is the "p-value" assuming false.
    for(i in which(!is.na(sibshare))){
        if(is.null(sensmat)){
            bHET=0.8; bHOM=1
        } else {
            #print(sensmat)
            #print(TYPE[i])
            #print(LEN[i])
            ss=getSens(sensmat,TYPE[i],LEN[i])
            #print(ss)
            bHET=ss$bHET; bHOM=ss$bHOM
        }
        p1sib[i] = pSmallerThanObservedSharingIfTrue(sibshare[i],sibcount[i],overallFreq[i],bHET=bHET,bHOM=bHOM)
        p0sib[i] = pLargerThanObservedSharingIfFalse(sibshare[i],sibcount[i],overallFreq[i])
    }
    L=log(p1sib/p0sib)
	
    sharing=data.frame(overallFreq,sibshare,sibcount,p0sib,p1sib,L)
    #print(sharing)
    #dev.off()
    sharing
}

getSens<-function(sensmat,ty,len){
    rowi=which(sensmat$TYPE==ty & sensmat$SIZE.LOWER<=len & sensmat$SIZE.UPPER > len)
    if(length(rowi)==0){
        rowi=nrow(sensmat)
    } 
    list(bHET=sensmat$SENS.HET[rowi], bHOM=sensmat$SENS.HOM[rowi])
}

pSmallerThanObservedSharingIfTrue<-function(obsp,n,f,bHET=0.8,bHOM=1){
        # y is the observed sharing count
        # n is the total units for counting y
        # f is the population call rate
        # beta is the sensitivity
        pShare=computeFSIBfromF(f,bHET,bHOM)
        obsy=obsp*n
        pbinom(obsy,n,pShare)
}

pLargerThanObservedSharingIfFalse<-function(obsp,n,f){
        1-pbinom(obsp*n,n,f)
}
    
getFamilyVSPopOverlap<-function(chr,chrst,chred,gridsize,vcf,fam,bHETs=NULL,bHOMs=NULL,plotPngPrefix="sharing",isSWAN=TRUE){
    ir=IRanges(start=seq(chrst,chrend,gridsize),width=gridsize)
    cat("Getting Family VS Population Overlap for chr ",chr," containing ",length(ir), " regions.\n",sep="")
    called.irs=vector("list",length(vcf))
    for(i in 1:length(vcf)){
        tvcf=vcf[[i]]
        tvcf$CHR=paste(tvcf$CHR)
        if(length(tvcf)>0){
            st=pmin(tvcf$START,tvcf$END)
            ed=pmax(tvcf$START,tvcf$END)
            tvcf$START=st
            tvcf$END=ed
            len=tvcf$END-tvcf$START
            sel=which(tvcf$CHR==chr)
            called.irs[[i]]=IRanges(start=tvcf$START[sel],end=tvcf$END[sel])
        } else {
            called.irs[[i]]=IRanges()
        }
    }
    
    ovmat=matrix(nrow=length(ir),ncol=length(vcf))
    for(ci in 1:length(vcf)){
        ovmat[,ci] = countOverlaps(ir,called.irs[[ci]])
    }
    ovmat=ovmat>0
    
    sibsets=findSibs(fam)
    famsets=findFams(fam)
    overallCount=rowSums(ovmat)  
    overallFreq=overallCount/nrow(fam)
    res=apply(ovmat,1,pSharing,sets=sibsets)
    sibshare=rep(0,length(res)); sibcount=rep(0,length(res))
    for(i in 1:length(res)){ 
        sibshare[i]=res[[i]]$pshare
        sibcount[i]=res[[i]]$denom
    }
    res=apply(ovmat,1,pSharing,sets=famsets)
    famshare=rep(0,length(res)); famcount=rep(0,length(res))
    for(i in 1:length(res)){ 
        famshare[i]=res[[i]]$pshare
        famcount[i]=res[[i]]$denom
    }
    
    lengths=c(start(ir)[1],width(ir))
    values=c(0,overallFreq)
    overallFreqRLE=Rle(values, lengths)
    values=c(0,famshare)
    famshareRLE = Rle(values, lengths)
    values=c(0,sibshare)
    sibshareRLE = Rle(values, lengths)
    values=c(0,famcount)
    famcountRLE=Rle(values,lengths)
    values=c(0,sibcount)
    sibcountRLE=Rle(values,lengths)
    
    sharestats=vector("list",length(vcf))
    fRLEinRanges<-function(ranges,tRLE,f,...){
        res=rep(NA,length(ranges))
        for(i in 1:length(ranges)){
            res[i]=f(tRLE[ranges[i]],...)
        }
        res
    }
    
    
    p1sib=rep(NA,length(sibshare))  # p1 is the "p-value" assuming true.
    p0sib=rep(NA,length(sibshare))  # p0 is the "p-value" assuming false.
    for(i in which(!is.na(sibshare))){
        p1sib[i] = pSmallerThanObservedSharingIfTrue(sibshare[i],sibcount[i],overallFreq[i],bHET=bHET,bHOM=bHOM)
        p0sib[i] = pLargerThanObservedSharingIfFalse(sibshare[i],sibcount[i],overallFreq[i])
    }
    p1fam=rep(NA,length(famshare))  # p1 is the "p-value" assuming true.
    p0fam=rep(NA,length(famshare))  # p0 is the "p-value" assuming false.
    for(i in which(!is.na(famshare))){
        p1fam[i] = pSmallerThanObservedSharingIfTrue(famshare[i],famcount[i],overallFreq[i],bHET=bHET,bHOM=bHOM)
        p0fam[i] = pLargerThanObservedSharingIfFalse(famshare[i],famcount[i],overallFreq[i])
    }
    
    logpvalrat.sib=log(p1sib/p0sib)
    logpvalrat.fam=log(p1fam/p0fam)
    values=c(0,logpvalrat.sib)
    siblogpvalratRLE=Rle(values,lengths)
    values=c(0,logpvalrat.fam)
    famlogpvalratRLE=Rle(values,lengths)
    values=c(0,p0sib)
    p0sibRLE=Rle(values,lengths)
    values=c(0,p1sib)
    p1sibRLE=Rle(values,lengths)
    values=c(0,p0fam)
    p0famRLE=Rle(values,lengths)
    values=c(0,p1fam)
    p1famRLE=Rle(values,lengths)
    
    
    
    for(ci in 1:length(vcf)){
        cat("Computing sharing stats for vcf ",ci, " out of ",length(vcf),"... \n")
        tvcf=vcf[[ci]]
        tvcf$CHR=paste(tvcf$CHR)
        sel=which(tvcf$CHR==chr)
        ranges=called.irs[[ci]]
        meanfamshare=fRLEinRanges(ranges,famshareRLE,mean,na.rm=TRUE)
        meansibshare=fRLEinRanges(ranges,sibshareRLE,mean,na.rm=TRUE)
        meanoverallFreq=fRLEinRanges(ranges,overallFreqRLE,mean,na.rm=TRUE)
        meanlogfampop=fRLEinRanges(ranges,log2(famshareRLE/overallFreqRLE),mean,na.rm=TRUE)
        meanlogsibpop=fRLEinRanges(ranges,log2(sibshareRLE/overallFreqRLE),mean,na.rm=TRUE)
        minp0sib = fRLEinRanges(ranges,p0sibRLE,min, na.rm=TRUE)
        maxp1sib = fRLEinRanges(ranges,p1sibRLE,max, na.rm=TRUE)
        maxlogpvalratsib=fRLEinRanges(ranges,siblogpvalratRLE,max,na.rm=TRUE)
        minp0fam = fRLEinRanges(ranges,p0famRLE,min, na.rm=TRUE)
        maxp1fam = fRLEinRanges(ranges,p1famRLE,max, na.rm=TRUE)
        maxlogpvalratfam=fRLEinRanges(ranges,famlogpvalratRLE,max,na.rm=TRUE)
                
        if(isSWAN) {
            sharestats[[ci]]=data.frame(CHR=tvcf$CHR[sel],START=tvcf$START[sel],END=tvcf$END[sel],TYPE=tvcf$TYPE[sel],METHOD=tvcf$METHOD[sel],
                                    meanfamshare,meansibshare,meanoverallFreq,meanlogfampop,meanlogsibpop,
                                    minp0sib,maxp1sib,maxlogpvalratsib,minp0fam,maxp1fam,maxlogpvalratfam)
        } else {
            sharestats[[ci]]=data.frame(tvcf[sel,],
                                    meanfamshare,meansibshare,meanoverallFreq,meanlogfampop,meanlogsibpop,
                                    minp0sib,maxp1sib,maxlogpvalratsib,minp0fam,maxp1fam,maxlogpvalratfam)        
        }
     }
    
    
    if(!is.null(plotPngPrefix)){
        png(paste(plotPngPrefix,1,"png",sep="."),height=900,width=400)
        sel=which(overallCount>0)
        par(mfrow=c(3,1))
        plot(overallCount[sel],sibshare[sel],xlab="Number of Carriers", ylab="Probability of Sib Sharing",xlim=c(0,155),ylim=c(0,1),cex.lab=1.5)
        abline(-1/155,1/155,col="red")
        grid()
        plot(overallCount[sel],famshare[sel],xlab="Number of Carriers", ylab="Probability of Family Sharing",cex.lab=1.5,xlim=c(0,155),ylim=c(0,1))
        grid()
        abline(-1/155,1/155,col="red")
        plot(log2(sibshare/overallFreq),log2(famshare/overallFreq), xlab="Log2(Sib/Population)",ylab="Log2(Family/Population",cex.lab=1.5)
        abline(0,1,col="red")
        grid()
        dev.off()
        
        png(paste(plotPngPrefix,2,"png",sep="."),height=900,width=1000)  
        par(mfrow=c(4,1))
        plot(start(ir),overallCount, ylim=c(0,155),xlab="Position",ylab="Number of Carriers",main="Number of Carriers")
        grid()
        plot(start(ir),sibshare,ylim=c(0,1),xlab="Position",ylab="Probabilty of Sib Sharing",main="Sib Sharing Ratio")
        grid()
        plot(start(ir),log2(sibshare/overallFreq),xlab="Position",ylab="Log2 Sib/Population",main="Log2 Sib to Population Sharing Ratio")
        abline(0,0,col="red")
        grid()
        plot(start(ir),log2(famshare/overallFreq),xlab="Position",ylab="Log2 Family/Population",main="Log2 Family to Population Sharing Ratio")
        abline(0,0,col="red")
        grid()
        dev.off()
        
        png(paste(plotPngPrefix,"logp","png",sep="."),height=900,width=1000)  
        par(mfrow=c(2,2))
        plot(log(p0sib),log(p1sib))
        abline(0,1,col="red")
        grid()
        plot(log(p0fam),log(p1fam))
        abline(0,1,col="red")
        grid()
        plot(logpvalrat.sib,logpvalrat.fam)
        abline(0,1,col="red")
        grid()
        hist(logpvalrat.sib,100,col="pink")
        dev.off()
    }
    
    list(sharestats=sharestats,sibshare=sibshare,famshare=famshare,famcount=famcount,sibcount=sibcount,overallFreq=overallFreq,ranges=ir,chr=chr)
}


computeNullSharing<-function(popFreq,fam){
#  popFreq=seq(0.05,1,0.05)

    sibsets=findSibs(fam)
    famsets=findFams(fam) 
    B=10000
    sibnull=matrix(nrow=B,ncol=length(popFreq))
    famnull=matrix(nrow=B,ncol=length(popFreq))
    for(pi in 1:length(popFreq)){
        cat(pi,"of",length(popFreq),"\n")
        p=popFreq[pi]
        n=nrow(fam)
        s=round(p*n)
        carriermat=matrix(nrow=B, ncol=n,data=0)
        for(b in 1:B){
            carriers=sample(n, s,replace=FALSE)
            carriermat[b,carriers]=1
        }
        sibnull[,pi]=apply(carriermat,1,pSharing,sets=sibsets)
        famnull[,pi]=apply(carriermat,1,pSharing,sets=famsets)
    }
    
    
    list(sibnull=sibnull,famnull=famnull,popFreq=popFreq,fam=fam)
}


findFams<-function(fam){
    famsets=list("vector",length=nrow(fam))
    for(i in 1:nrow(fam)){
        famsets[[i]]=which(fam$FamID==fam$FamID[i])
        famsets[[i]] = setdiff(famsets[[i]],i)
    }
    famsets
}


findSibs<-function(fam){
    sibsets=list("vector",length=nrow(fam))
    for(i in 1:nrow(fam)){
        sibsets[[i]]=which(fam$Father==fam$Father[i] & fam$Mother==fam$Mother[i])
        sibsets[[i]] = setdiff(sibsets[[i]],i)
    }
    sibsets
}

# callvec=ovmat[238,]
pSharing=function(callvec,sets){
    sel=which(callvec==TRUE)
    if(length(sel)==0){
        return(list(pshare=NA,denom=0))
    }
    sumshare=0
    denom=0
    for(i in 1:length(sel)){
        thisset=sets[[sel[i]]]
        if(length(thisset)>0){
            sumshare=sumshare+sum(callvec[thisset])
            denom=denom+length(thisset)
        }
    }
    list(pshare=sumshare/denom, denom=denom)
}

hasOverlap<-function(ir1,ir2,MAX.OFFSET=200){
    st1=IRanges(start=start(ir1)-MAX.OFFSET, end=start(ir1)+MAX.OFFSET)
    st2=IRanges(start=start(ir2),width=1)
    ovSt=findOverlaps(st1,st2)
    ed1=IRanges(start=end(ir1)-MAX.OFFSET, end=end(ir1)+MAX.OFFSET)
    ed2=IRanges(start=end(ir2),width=1)
    ovEd=findOverlaps(ed1,ed2)
    
    inter=intersect(ovSt,ovEd)
    temp=as.matrix(inter)
    ovBoth=rep(FALSE,length(ir1))
    ovBoth[unique(temp[,1])]=TRUE
    ovBoth
    
    
#    st=start(ir1)
#    ed=end(ir1)
#    st2=start(ir2)
#    ed2=end(ir2)
#    win=round(MAX.OFFSET/2)
#    irst=IRanges(start=st-win,end=st+win)
#    irst2=IRanges(start=st2-win,end=st2+win)
#    cost=countOverlaps(irst,irst2)
#    ired=IRanges(start=ed-win,end=ed+win)
#    ired2=IRanges(start=ed2-win,end=ed2+win)
#    coed=countOverlaps(ired,ired2)
#    ov=(cost>0) & (coed>0)
#    ov
}

shadedHistogram<-function(score,shade,horizontal=TRUE,breaks=NULL,shadeColor="blue",nonshadeColor="gray",label="Score",...){
    if(!is.null(breaks)){
        score=threshold(score, min(breaks),max(breaks))
        hi=hist(score, breaks=breaks,plot=FALSE)
    } else {
        hi=hist(score, plot=FALSE)
        breaks=hi$breaks
    }
    

    
    if(horizontal){
        plot(rep(0,length(breaks)),breaks,xlab="Count",ylab=label,type="l",xlim=c(0,max(hi$counts)),...)
    } else {
        plot(breaks,rep(0,length(breaks)),ylab="Count",xlab=label,type="l",ylim=c(0,max(hi$counts)),...)    
    }
    shaded=rep(0,0)
    total=rep(0,0)
    for(bi in 1:(length(breaks)+1)){
        countTotal=sum(as.numeric(sel))
        sel=which(score>breaks[bi] & score<= breaks[bi+1])
        countTotal=length(sel)
        countShade=sum(shade[sel])
        if(horizontal){
            rect(0,breaks[bi],countTotal,breaks[bi+1],col=nonshadeColor)
            rect(0,breaks[bi],countShade,breaks[bi+1],col=shadeColor)
        } else {
            rect(breaks[bi],0,breaks[bi+1],countTotal,col=nonshadeColor)
            rect(breaks[bi],0,breaks[bi+1],countShade,col=shadeColor)        
        }
        shaded=c(shaded,countShade)
        total=c(total,countTotal)
    }
    list(shaded=shaded,total=total,breaks=breaks)
}

computefs<-function(p,bHET,bHOM,doplot=TRUE){
    pHET=2*p*(1-p)
    pHOM=p^2
    pCALLSIBgHET=(bHET/2)*(1+p-p^2)+(bHOM/4)*(p+p^2)
    pCALLSIBgHOM=bHOM*(p+0.25*(1-p)^2)+(bHET/2)*(1-p^2)
    f=pHET*bHET+pHOM*bHOM     # observed call rate.
    pHETgCALL=(bHET*pHET)/f
    pHOMgCALL=1-pHETgCALL
    
    #fSib1=bHET/2 + f -f^2/(2*bHET)  # sib carrier rate from previous equation
    fSib1=bHET/2 + f/2 -f^2/(8*bHET)  # sib carrier rate from previous equation
    fSib2=pHETgCALL*pCALLSIBgHET+pHOMgCALL*pCALLSIBgHOM
    if(doplot){
        plot(f, fSib1,type="l",xlim=c(0,1),ylim=c(0,1), 
            xlab="P(Call)", ylab="P(Call | Call in Sib)",
            main=paste("HET: ",bHET,",      HOM: ",bHOM,sep=""))
        lines(f,fSib2,type="l",col="blue")
        grid()
        abline(0,1,col="gray")
        legend(x="bottomright",lty=c(1,1),col=c("black","blue"),legend=c("Approx","Truth"))
    }
    list(f=f,fSib1=fSib1,fSib2=fSib2)
}


computeFSIBfromF<-function(f,bHET,bHOM,verbose=FALSE){
    if(f==0){
        return(0)
    }
    if(f>bHOM){
        fSib=f
        if(verbose) cat("Warning: f is larger than HOM sensitivity, setting fSib to ", fSib,".\n",sep="")
        return(fSib)
    }
    a=bHOM-2*bHET
    b=2*bHET
    c=-f
    if(b^2<4*a*c){
        if(verbose) cat("\t\tWarning: can not solve for p, resolve to HET approximation.\n")
        return(bHET/2 + f/2 -f^2/(8*bHET))
    }
    if(a==0){
        p1=f/(2*bHET)
        p2=p1
    } else {
        p1=(-b+sqrt(b^2-4*a*c))/(2*a)
        p2=(-b-sqrt(b^2-4*a*c))/(2*a)
    }
    fSib.1=0
    fSib.2=0
    if(p1>0 & p1<=1){
        fSib.1=computefs(p1,bHET,bHOM,doplot=FALSE)$fSib2
    }
    if(p2>0 & p2<=1){
        fSib.2=computefs(p2,bHET,bHOM,doplot=FALSE)$fSib2
    }
    fSib=0
    if(fSib.1>=f){
        fSib=fSib.1
    }
    if(fSib.2>= max(f,fSib.1)){
        fSib=fSib.2
    }
    if(fSib==0){
        if(verbose) cat("Warning: fSib.1=",fSib.1, ", fSib.2=",fSib.2,", f=",f,"\n")
        fSib=f        
    }
    fSib
}


threshold<-function(y,miny,maxy){
    pmax(pmin(y,maxy),miny)
}

getElementFromListOfLists<-function(ll,elementNum){
    # li is a list, where each element is another list of K things.
    getElement<-function(lli,elementNum){
        lli[[elementNum]]
    }
    res=lapply(ll,getElement,elementNum)
    unlist(res)
}
