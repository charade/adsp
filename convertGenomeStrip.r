#setwd("~/myadsp")
fnames=read.table("PedigreeAnalysis/WGS_SRR_IDs.csv",header=TRUE,sep=",")

vcffile="PedigreeAnalysis/GenomeStrip/adsp.wgs.all.genome_strip.cnv.ann.vcf"
con=file(vcffile, open = "r")

while (length(oneLine <- readLines(con, n = 1, warn = FALSE)) > 0) {
    # Read until the header line.
    if(substr(oneLine,1,6)=="#CHROM") break
} 

temp=substr(oneLine,2,nchar(oneLine))
header=strsplit(temp,"\t")[[1]]
SampleIDs=header[10:length(header)]

CHROM=rep("",0)
POS=rep(0,0)
ID=rep("",0)
ALT=rep("",0)
FILTER=rep("",0)
INFO=rep("",0)
FORMAT=rep("",0)
QUAL=rep("",0)
CN=matrix(nrow=0,ncol=length(SampleIDs))
FT=matrix(nrow=0,ncol=length(SampleIDs))
CNQ=matrix(nrow=0,ncol=length(SampleIDs))

#GT=matrix(nrow=0,ncol=length(SampleIDs))
#PL=matrix(nrow=0,ncol=length(SampleIDs))
#GQ=matrix(nrow=0,ncol=length(SampleIDs))


getFields<-function(str,format){
    fieldnames=strsplit(format,":")[[1]]
    fields=strsplit(str,":")[[1]]
    a=list()
    for(i in 1:length(fieldnames)){
        a[[fieldnames[i]]]=fields[i]
    }
    a
}

while (length(oneLine <- readLines(con, n = 1, warn = FALSE)) > 0) {
    temp=strsplit(oneLine,"\t")[[1]]
    cat("Read line number ", length(CHROM)+1,"\n")
    CHROM=c(CHROM,temp[1])
    POS=c(POS,as.numeric(temp[2]))
    ID=c(ID,temp[3])
    ALT=c(ALT,temp[5])
    QUAL=c(QUAL,temp[6])
    FILTER=c(FILTER,temp[7])
    INFO=c(INFO,temp[8])
    FORMAT=c(FORMAT,temp[9])
    gt=rep("",0); cn=rep(0,0); gq=rep("",0); pl=rep("",0); cnq=rep(0,0); ft=rep("",0)
    for(j in 1:length(SampleIDs)){
        #cat(j,"\n")
        res=getFields(temp[9+j],temp[9])
        if(!is.null(res$GT)){
            gt=c(gt, res$GT)
        } else {
            gt=c(gt,"")
        }
        if(!is.null(res$CN)){
            cn=c(cn, as.integer(res$CN))
        } else {
            cn=c(cn,2)
        }
        if(!is.null(res$gq)){
            gq=c(gq, res$GQ)
        } else {
            gq=c(gq,"")
        }
        if(!is.null(res$PL)){
            pl=c(pl, res$PL)
        } else {
            pl=c(pl,"")
        }
        if(!is.null(res$CNQ)){
            cnq=c(cnq, res$CNQ)
        } else {
            cnq=c(cnq,"")
        }
        if(!is.null(res$FT)){
            ft=c(ft, res$FT)
        } else {
            ft=c(ft,"")
        }
    }
#    GT=rbind(GT,gt); GQ=rbind(GQ,gq); PL=rbind(PL,pl); 
    CN=rbind(CN,cn); CNQ=rbind(CNQ,cnq); FT=rbind(FT,ft)
} 


close(con)

save.image(paste("PedigreeAnalysis/GenomeStrip/GenomeStrip",length(SampleIDs),"samples","RData",sep="."))
