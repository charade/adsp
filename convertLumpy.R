#convert Pindel calls into Pindel.$center.RData file
setwd("~/adsp_rawcalls")
FamilyFile="~/adsp_rawcalls/ADSP_WGSfam_pilot.srr.txt"
famtab=read.table(FamilyFile,header=TRUE,sep="\t")
centers=c("Baylor","Broad","WashU")
Caller="Lumpy"
CallDir=paste("~/adsp_rawcalls/",Caller,"/",sep="")
DataDir=paste("~/adsp_dscore/PedigreeAnalysis/",sep="")
#ADSP2SRRFile="~/adsp_rawcalls/ADSP2SRR.txt"

#ADSP2SRR=read.table(ADSP2SRRFile)
#colnames(ADSP2SRR)=c("SampleID","SRR")
#ADSP2SRR$SRR[match(famtab$SampleID,ADSP2SRR$SampleID)]

for(ci in seq_along(centers)){
  vcf=list()
  center=centers[ci]
  fam=famtab[famtab$SeqCtr==center,]  #pilot samples
  cat("c=",center,"n=",nrow(fam),"\n")
  print(head(fam))
  for(i in seq_len(nrow(fam))){
    #cat("SampleID=",as.character(fam$SampleID[i]),"\n")
    #srr=ADSP2SRR$SRR[match(fam$SampleID[i],ADSP2SRR$SampleID)]
    srr=as.character(fam$SRR[i])
    fpat=paste("*.del.bed.gz$",sep="")
    fpath=paste(CallDir,center,"/",sep="")
    cat("fpat=",fpat,"\n")
    cat("fpath=",fpath,"\n")
    cat("srr=",srr,"\n")
    allFiles=list.files(path=fpath, pattern=fpat)
    CallFile=paste(fpath, allFiles[grep(srr,allFiles)][1], sep="")
    cat("CallFile=",CallFile,"\n")
    vcf[[i]]=read.table(CallFile)
    colnames(vcf[[i]])=c("CHR","START","END","TYPE","SCORE")
  }
  RDataFile=paste(DataDir, paste(Caller,center,"raw.RData",sep="."), sep="")
  cat("RDataFile=",RDataFile,"\n")
  save(file=RDataFile,list=c("fam","vcf"))
}
