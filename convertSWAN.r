
args = commandArgs(trailingOnly=TRUE)
print(args)
center = args[1]       # center, Baylor Broad or WashU.
centerdir = args[2]     # directory containing the samples of the center.
famfile=args[3]         # family file.
removeSoftClip=FALSE
removeSexChr=TRUE
#
#center="Baylor"
#centerdir="baylor_rj"
#famfile="ADSP_WGSfam.txt"


source("adsplib.r")
library(GenomicRanges)

outfile=paste("SWAN",center,"sharing","RData",sep=".")

#####################################################
# Get family file, get vcfs
#####################################################
fam=read.table(famfile,header=TRUE,sep="\t",stringsAsFactors=FALSE)
throw=which(fam$SeqCtr!=center)
fam=fam[-throw,]
vcf=vector("list",nrow(fam))
throw=rep(0,0)
failed=matrix(nrow=0,ncol=2)

for(i in 1:nrow(fam)){
### One directory per sample
    dirpath=paste(centerdir,"/",fam$SampleID[i],sep="")
    cat("Loading sample ", dirpath,"\n")
    temp=dir(dirpath)
    
    if(length(temp)==0){
       dirpath=paste(centerdir,"/",fam$SampleID[i],"_Illumina",sep="")
        cat("\t\tTrying ", dirpath,"\n")
    
        temp=dir(dirpath)
    }
    
    if(length(temp)==0){
        cat("\n### Row ",i,fam$SampleID[i],"Directory not found.\n\n")
        throw=c(throw,i)
    } else {
        files=temp[grep(".conf.bed",temp)]
        
        if(length(files)==0){
            cat("\n### Row ",i,fam$SampleID[i],"not processed.\n\n")
            throw=c(throw,i)
        } else {
            for(f in 1:length(files)){
                cat("Reading in file ", f,":",files[f],"\n",sep="")
                temp=readVCF(paste(dirpath,"/",files[f],sep=""))
                if(f==1){
                    vcf[[i]]=temp
                } else {
                    vcf[[i]] = rbind(vcf[[i]],temp)
                }
            }
            cat("Read in ", nrow(vcf[[i]])," entrees.\n",sep="")
    
            if(removeSoftClip==1) vcf[[i]]=vcf[[i]][vcf[[i]]$METHOD!="sclip",]
            if(removeSexChr==1){ 
                vcf[[i]]=vcf[[i]][vcf[[i]]$CHR!="Y",]
            }
            cat("After filtering, there are ", nrow(vcf[[i]])," entrees remaining in VCF.\n",sep="")
        }
    }


#
#    ### When all vcfs are in same directory:
#    dirpath=centerdir
#    chroms=c(paste(1:22),"X")
#    for(ch in 1:length(chroms)){
#        file=paste(fam$SampleID[i],".chr",chroms[ch],".conf.bed",sep="")
#        temp=try(readVCF(paste(dirpath,"/",file,sep="")))
#        if(class(temp)!="try-error"){
#            if(ch==1){
#                vcf[[i]]=temp
#            } else {
#                vcf[[i]]=rbind(vcf[[i]],temp)
#            }
#            cat("Read in ", nrow(vcf[[i]])," entrees.\n",sep="")
#        } else {
#            failed=rbind(failed,c(fam$SampleID,chroms[ch]))
#        }
#    }
#    if(nrow(vcf[[i]])==0) throw=c(throw,i)


}
cat("Threw out samples:\n")
print(fam[throw,])
if(length(throw)>0){
    fam=fam[-throw,]
    vcf=vcf[-throw]
}

cat("\n\n\nRead in ",nrow(fam)," samples.\n\n\n")


save.image(outfile)
