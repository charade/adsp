## sh swjoin.sh
## copy qsub lines and try it
## if it's working: 
## 	for f in `ls */*.swjoin.pbs`; do qsub -q plus -r swjoin $f; done;
## the logs are in .swjoin.log 


for file in `ls -d */ | grep CU`; 
do 
	for chr in `echo {1..22}`; 
	do 
		bn=${file%/}; 
		pf=$bn/$bn; 
		t=8;	# THRESHOLD FOR LCD 
		cmd="swan_join.R -q -c $chr -i $pf.chr$chr.swan.txt.gz -u track=lCd,method=empr,thresh=$t,sup=100,gap=100 -o $pf.chr$chr $HOME/ws/hg/hg19/human_g1k_v37.fasta $pf.chr$chr.bam >$pf.chr$chr.t$t.swjoin.log 2>&1"; 
		
		# This creates .pbs file in each sample directory.
		echo $cmd >$pf.chr$chr.t$t.swjoin.pbs; 
		
		# This doesn't do anything, just shows the run commands at prompt.
		echo qsub -q plus -r swjoin.t$t $pf.chr$chr.t$t.swjoin.pbs; 
	done; 
done; 



